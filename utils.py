import os
import sys
import logging
import ccxt


def get_logger(name, level='DEBUG', logpath=None, logfile=os.path.basename(sys.argv[0])):
    level = getattr(logging, level.upper())
    logger = logging.getLogger(name)
    logger.propagate = False
    logger.setLevel(level)
    stdout_handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(
            '%(asctime)s - %(name)s -%(filename)s[line:%(lineno)d] - %(levelname)s - %(message)s'
        )
    stdout_handler.setFormatter(formatter)
    logger.addHandler(stdout_handler)
    if logpath:
        logfile = os.path.basename(logfile)
        log_path = os.path.join(logpath, logfile + '.log')
        file_handler = logging.FileHandler(log_path)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    return logger


class Trades:

    def __init__(self, logger):
        self.logger = logger

    def get(self, exchange_id, market):
        try:
            exchange = getattr(ccxt, exchange_id)()
        except AttributeError:
            self.logger.error(f'wrong exchange id - {exchange_id}')
            return None
        if not exchange.has['fetchTrades']:
            self.logger.error(f'{exchange_id} has not fetchTrades endpoint')
        try:
            trades = exchange.fetch_trades(market)
        except ccxt.ExchangeError as e:
            self.logger.error(f'{exchange_id} - {market} order book receiving error: {str(e)}')
            return None
        except ccxt.NetworkError as e:
            self.logger.error(f'{exchange_id} - {market} network error: {str(e)}')
            return None
        return trades

