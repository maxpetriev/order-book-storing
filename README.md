## Description
For each exchange and market from `settings.py` app trying to get last 100 trades every `GET_TIMEOUT` seconds and 
store in to redis. Unique trades will be stored to mongo  every `SAVE_TIMEOUT` seconds. 
                                             
## HOW TO USE
1. install dependencies using `pipenv sync`.
2. install mongo and redis or just install docker-compose and run `docker-compose up -d`.
3. create `settings_local.py` and add exchanges and markets in following format:

```python
EXCHANGES = [
    {
        'id': 'binance', # ccxt exchange id
        'market': 'ETH/BTC' # market name
    },
    {
        'id': 'bittrex',
        'market': 'ETH/BTC'
    }
]
```

4. run `python run.py --get` to start getting trades and temporarily save in to redis.
5. run  `python run.py --save` to start storing unique trades to mongo. 

## Prerequisites
1. python3.6
2. pipenv
3. mongodb
4. redis
