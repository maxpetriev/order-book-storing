import os


MONGO_CONNECTION_STRING = os.environ.get('MONGO_CONNECTION_STRING', 'mongodb://localhost:27017/trades_history')
REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', 6379)
LOG_PATH = os.environ.get('LOG_PATH', '.')
GET_TIMEOUT = os.environ.get('GET_TIMEOUT', 30) # every 30 seconds
SAVE_TIMEOUT = os.environ.get('SAVE_TIMEOUT', 60*5) # every 5 minutes

EXCHANGES = [
    {
        'id': 'binance',
        'market': 'ETH/BTC'
    },
    {
        'id': 'bittrex',
        'market': 'ETH/BTC'
    }
]

try:
    from settings_local import *
except ImportError as e:
    pass
