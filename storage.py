import pymongo
import redis
import re
import json


class MongoDB:
    TRADES_HISTORY_COLLECTION = 'TradesHistory'

    def __init__(self, connection_string, logger):
        self.db = self._get_connection(connection_string)
        self.logger = logger

    @staticmethod
    def _get_connection(connection_string):
        client = pymongo.MongoClient(connection_string)
        db = client.get_database()
        return db

    def check_index(self):
        self.db[self.TRADES_HISTORY_COLLECTION].create_index([
            ('Exchange', pymongo.ASCENDING),
            ('Market', pymongo.ASCENDING),
            ('Timestamp', pymongo.ASCENDING),
            ('Price', pymongo.ASCENDING),
            ('Amount', pymongo.ASCENDING)],
            unique=True)

    def save(self, exchange_id, market, trades):
        for trade in trades:
            doc = {
                'Exchange': exchange_id,
                'Market': market,
                'Id': trade.get('id', None),
                'Timestamp': trade['timestamp'],
                'DateTime': trade['datetime'],
                'Side': trade.get('side', None),
                'Price': trade['price'],
                'Amount': trade['amount']
            }
            try:
                self.logger.debug('Success saving')
                self.db[self.TRADES_HISTORY_COLLECTION].insert_one(doc)
            except pymongo.errors.DuplicateKeyError:
                self.logger.debug('DuplicateKeyError')
        self.logger.info(
            f"Saving trades to to mongo exchange - {exchange_id} market - {market}")


class Redis:

    def __init__(self, host, port, logger, db=0):
        self.connection = redis.StrictRedis(host=host, port=port, db=db)
        self.logger = logger

    @staticmethod
    def get_list_name(exchange_id, market):
        ticker = re.sub(r'[/-]','_', market)
        return f'{exchange_id}_{ticker}'

    def save(self, exchange_id, market, trades):
        list_name = self.get_list_name(exchange_id, market)
        self.logger.info(f'Saving trades to redis exchange - {exchange_id} market - {market}')
        self.connection.lpush(list_name, json.dumps(trades))

    def get_recent_trades(self, exchange_id, market):
        list_name = self.get_list_name(exchange_id, market)
        trades = self.connection.rpop(list_name)
        if trades:
            return json.loads(trades)
        else:
            return None
