import argparse
import time

from utils import get_logger, Trades
from storage import MongoDB, Redis
import settings


parser = argparse.ArgumentParser(description='Trying to get trades every GET_TIMEOUT seconds and '
                                             'save unique trades to mongo every SAVE_TIMEOUT seconds')
parser.add_argument('--get', help='starts getting trades', action='store_true')
parser.add_argument('--save', help='starts saving trades', action='store_true')
args = parser.parse_args()


def get(logger, redis):
    trades = Trades(logger)
    for exchange in settings.EXCHANGES:
        trades_list = trades.get(exchange['id'], exchange['market'])
        if trades_list:
            redis.save(exchange['id'], exchange['market'], trades_list)


def save(redis, mongo):
    for exchange in settings.EXCHANGES:
        trades = redis.get_recent_trades(exchange['id'], exchange['market'])
        while trades:
            mongo.save(exchange['id'], exchange['market'], trades)
            trades = redis.get_recent_trades(exchange['id'], exchange['market'])


if __name__ == '__main__':
    if args.get:
        logger = get_logger('GET worker', logpath=settings.LOG_PATH)
        redis = Redis(settings.REDIS_HOST, settings.REDIS_PORT, logger)
        while True:
            get(logger, redis)
            time.sleep(settings.GET_TIMEOUT)

    if args.save:
        logger = get_logger('SAVE worker', logpath=settings.LOG_PATH)
        redis = Redis(settings.REDIS_HOST, settings.REDIS_PORT, logger)
        mongo = MongoDB(settings.MONGO_CONNECTION_STRING, logger)
        mongo.check_index()
        while True:
            save(redis, mongo)
            time.sleep(settings.SAVE_TIMEOUT)
